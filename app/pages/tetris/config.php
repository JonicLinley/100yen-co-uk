<?php
return [
    'description' => 'Watch the blocks fall. You will be pleased.',
    'cover_image' => '/app/pages/tetris/images/cover.png',
    'icon' => '/app/pages/tetris/images/icon.png',
    'title' => 'Do, dododo, dododo...',
];
