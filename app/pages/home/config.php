<?php
return [
    'cover_image' => '/app/pages/home/images/cover.png',
    'description' => 'I’m Jonic. I make things. This is my website.',
    'icon' => '/app/pages/home/images/icon.png',
    'title' => '👋 Hiya',
];
